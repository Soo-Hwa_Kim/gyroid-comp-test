function plotCorrectedStressStrain(stress_strain_cell, n, ExperimentalData)

plot_dir = ExperimentalData.stress_strain_plot_dir;
plot_fname = sprintf('StressStrain%i',ceil(n/5));

fig1=figure;
plot(stress_strain_cell{1}(:,2), stress_strain_cell{1}(:,1),'LineWidth',2);hold on
plot(stress_strain_cell{2}(:,2), stress_strain_cell{2}(:,1),'LineWidth',2)
plot(stress_strain_cell{3}(:,2), stress_strain_cell{3}(:,1),'LineWidth',2)
plot(stress_strain_cell{4}(:,2), stress_strain_cell{4}(:,1),'LineWidth',2)
plot(stress_strain_cell{5}(:,2), stress_strain_cell{5}(:,1),'LineWidth',2)
xlabel('Strain %')
ylabel('Stress MPa')
xlim([0 inf])
ylim([0 inf])

savefig([plot_dir, plot_fname, '.fig']);
set(fig1,'Units', 'Inches');
pos = get(fig1, 'Position');
set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig1, [plot_dir, plot_fname, '.pdf'], '-dpdf', '-r0') %save as pdf

end