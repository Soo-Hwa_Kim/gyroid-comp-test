function ExperimentalData = importExperimentalData

data_dir = [cd, '\..\..\Experiments\ExperimentalData\'];
csv_files = locateCSVFiles(data_dir);


for n = 1:length(csv_files)
    
    this_filename = csv_files(n).name;
    fulldir = fullfile(data_dir, this_filename);
    filename_no_ext = getFileNameNoExt(this_filename);
    this_table = readtable(fulldir);
    if contains(filename_no_ext, 'CompTest')
        part_fieldname = getPartFieldname(filename_no_ext);
        ExperimentalData.IndividualParts.(part_fieldname).Compression = table2array(this_table(:,2:end));
    elseif contains(filename_no_ext, 'filament')
        ExperimentalData.density.filamenttbl = this_table;
        density_arr = table2array(this_table(:,2:end));
        length_cm = density_arr(1,1)/10;
        diameter_cm = density_arr(2,:)/10;
        weight_g = density_arr(3,:);
        vol_cm3 = pi .* (diameter_cm/2).^2 .* length_cm;
        density_g_cm3 = weight_g ./ vol_cm3;
        ExperimentalData.density.meandensitygcm3 = mean(density_g_cm3);
        ExperimentalData.density.stddevdensitygcm3 = std(density_g_cm3);
    else
        ExperimentalData = parseOtherData(this_table, filename_no_ext, ExperimentalData);
    end
    
end

ExperimentalData = getOutputDirs(ExperimentalData);

end

function csv_files = locateCSVFiles(data_dir)

csv_files = dir([data_dir, '/*.csv']);
csv_files = csv_files(~([csv_files.isdir]));

end

function filename_no_ext = getFileNameNoExt(filename)

dot_idx = find(filename == '.');
filename_no_ext = filename(1:dot_idx-1);
filename_no_ext = filename_no_ext(find(~isspace(filename_no_ext))); % trim all whitespaces

end

function part_fieldname = getPartFieldname(filename_no_ext)

fieldname = filename_no_ext(10:end);
part_fieldname = strrep(fieldname, '-', '_');
part_fieldname = ['PART', part_fieldname]; 

end

function ExperimentalData = parseOtherData(this_table, filename_no_ext, ExperimentalData)
this_data = table2array(this_table(:,2:end));
exptdata_fieldnames = fieldnames(ExperimentalData.IndividualParts);
if length(this_data) ~= length(exptdata_fieldnames)
    keyboard
end

for n = 1:length(exptdata_fieldnames)
   ExperimentalData.IndividualParts.(exptdata_fieldnames{n}).(filename_no_ext) = this_data(n,:);
end

end