function [stress_MPa, strain] = changeCompDataToStressStrain(compression_array, cross_area_mm2, z_mm)

stress_MPa = compression_array(:,2) ./ cross_area_mm2 * 1000; % units:MPa
strain = compression_array(:,1) ./ z_mm;

end