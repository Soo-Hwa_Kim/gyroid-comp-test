function lin_window_idx = findLinearWindow(compression_array, ExperimentalData, this_field)

cxydxy = ExperimentalData.IndividualParts.(this_field).abcd(5:end);

start_data_idxs = find(compression_array(:,1)>=cxydxy(1));
start_data_idx = min(start_data_idxs);

end_data_idxs = find(compression_array(:,1)<=cxydxy(3));
end_data_idx = max(end_data_idxs);

lin_window_idx = [start_data_idx, end_data_idx];

end