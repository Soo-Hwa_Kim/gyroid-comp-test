function ExperimentalData = getOutputDirs(ExperimentalData)

ExperimentalData.resid_plot_dir = [pwd, '\Outputs\ResidualPlots\'];
ExperimentalData.ga_plot_dir = [pwd, '\Outputs\GAConstPlots\'];
ExperimentalData.comp_plot_dir = [pwd, '\Outputs\ComparisonPlots\'];
ExperimentalData.stress_strain_plot_dir = [pwd, '\Outputs\StressStrainPlots\'];


end