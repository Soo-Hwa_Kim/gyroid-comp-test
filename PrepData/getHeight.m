function [z_mm, ExperimentalData] = getHeight(ExperimentalData, this_field)

z_mm = mean(ExperimentalData.IndividualParts.(this_field).ZLengths);
ExperimentalData.IndividualParts.(this_field).stddev.z = std(ExperimentalData.IndividualParts.(this_field).ZLengths);
ExperimentalData.IndividualParts.(this_field).mean_z_lengths = z_mm;

end