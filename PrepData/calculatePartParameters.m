function ExperimentalData = calculatePartParameters(ExperimentalData)

exptdata_fieldnames = fieldnames(ExperimentalData.IndividualParts);

for n = 1:length(exptdata_fieldnames)
    
    fieldname = exptdata_fieldnames{n};
    cross_area_mm2 = calculateCrossArea(ExperimentalData.IndividualParts.(fieldname));
    [stress_MPa, strain] = calculateStressStrain(ExperimentalData.IndividualParts.(fieldname), cross_area_mm2);
    yield_strength_MPa = findYieldStrength(stress_MPa, strain, fieldname);
    youngs_mod_MPa = calculateYoungsModulus(stress_MPa, strain, yield_strength_MPa);
    final_strength_MPa = findFinalStrength(stress_MPa, strain);
    
end

end

