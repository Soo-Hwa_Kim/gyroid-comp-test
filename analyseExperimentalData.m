%%function analyseExperimentalData

close all; clear all

prelim_flag = false;

ExperimentalData = importExperimentalData;

if prelim_flag
    preliminaryAnalysis(ExperimentalData.IndividualParts, ExperimentalData.density.meandensitygcm3);
end

[youngs_mod_MPa, yield_str_MPa, indiv_vol_frac, ExperimentalData] = obtainMechProps(ExperimentalData);
%%
[C, n, ExperimentalData] = calculateGAConstants(youngs_mod_MPa, yield_str_MPa, indiv_vol_frac, ExperimentalData);

compareExptSim(C, n, ExperimentalData);

%ExperimentalData = calculatePartParameters(ExperimentalData);



%end