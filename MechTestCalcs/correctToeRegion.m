function stress_strain_corr_mat = correctToeRegion(lin_window_idx, youngs_mod_MPa, y_int, stress_MPa, strain)

x_int = -y_int / youngs_mod_MPa;
strain_corr = strain(lin_window_idx(1):end) - x_int;
stress_MPa_corr = stress_MPa(lin_window_idx(1):end);

try
    strain_corr = [0; strain_corr];
    stress_MPa_corr = [0; stress_MPa_corr];
catch
    strain_corr = [0, strain_corr]';
    stress_MPa_corr = [0, stress_MPa_corr]';
end

if length(stress_MPa_corr) ~= length(strain_corr)
    keyboard
end

stress_strain_corr_mat = [stress_MPa_corr, strain_corr];

end