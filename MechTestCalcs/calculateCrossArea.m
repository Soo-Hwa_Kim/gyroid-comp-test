function [cross_area_mm2, ExperimentalData] = calculateCrossArea(ExperimentalData, this_field)

ExperimentalData.IndividualParts.(this_field).mean_x_lengths = mean(ExperimentalData.IndividualParts.(this_field).XLengths);
ExperimentalData.IndividualParts.(this_field).mean_y_lengths = mean(ExperimentalData.IndividualParts.(this_field).YLengths);
ExperimentalData.IndividualParts.(this_field).stddev.x = std(ExperimentalData.IndividualParts.(this_field).XLengths);
ExperimentalData.IndividualParts.(this_field).stddev.y = std(ExperimentalData.IndividualParts.(this_field).YLengths);

cross_area_mm2 = ExperimentalData.IndividualParts.(this_field).mean_x_lengths * ExperimentalData.IndividualParts.(this_field).mean_y_lengths;

end