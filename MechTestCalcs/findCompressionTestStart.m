function [comp_start, displ_diff] = findCompressionTestStart(stress_MPa, displ_array_mm)

idx_start = min(find(stress_MPa>0.05));
displ_diff = displ_array_mm(idx_start);

end