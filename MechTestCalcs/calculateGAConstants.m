function [C, n, ExperimentalData] = calculateGAConstants(youngs_mod_MPa, yield_str_MPa, indiv_vol_frac, ExperimentalData)

youngs_mod_MPa = reshape8by5(youngs_mod_MPa);
mean_YM = mean(youngs_mod_MPa, 2);
stddev_YM = std(youngs_mod_MPa, 0, 2);

indiv_vol_frac = reshape8by5(indiv_vol_frac);
mean_VF = mean(indiv_vol_frac, 2);
stddev_VF = std(indiv_vol_frac, 0, 2);

[C, n, coeff_stddev] = getGAConst(mean_YM, mean_VF);

fitted_YM = C .* mean_VF .^ n;

fig1 = figure;
subplot(211)
plot(mean_VF, mean_YM, '.'); hold on
plot(mean_VF, fitted_YM, '-')
xlabel('Volume Fraction')
ylabel('Youngs Modulus MPa')
subplot(212)
plot(mean_VF, mean_YM-fitted_YM, '.')
xlabel('Volume Fraction')
ylabel('Residuals')

savefig([ExperimentalData.ga_plot_dir, 'GAResid.fig']);
set(fig1,'Units', 'Inches');
pos = get(fig1, 'Position');
set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig1, [ExperimentalData.ga_plot_dir, 'GAResid.pdf'], '-dpdf', '-r0') %save as pdf

fig2 = figure;
loglog(mean_VF, mean_YM, '.'); hold on
loglog(mean_VF, fitted_YM, '-')
xlabel('Volume Fraction')
ylabel('Youngs Modulus MPa')
title('Log scaled plot of GA fit')

savefig([ExperimentalData.ga_plot_dir, 'GALogLog.fig']);
set(fig2,'Units', 'Inches');
pos = get(fig2, 'Position');
set(fig2, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig2, [ExperimentalData.ga_plot_dir, 'GALogLog.pdf'], '-dpdf', '-r0') %save as pdf

ExperimentalData.YoungsModulusMPa.youngs_mod_MPa = youngs_mod_MPa;
ExperimentalData.YoungsModulusMPa.mean_YM = mean_YM;
ExperimentalData.YoungsModulusMPa.stddev_YM = stddev_YM;

ExperimentalData.VolumeFraction.indiv_vol_frac = indiv_vol_frac;
ExperimentalData.VolumeFraction.mean_VF = mean_VF;
ExperimentalData.VolumeFraction.stddev_VF = stddev_VF;

ExperimentalData.GAConstants.C = C;
ExperimentalData.GAConstants.n = n;
ExperimentalData.GAConstants.coeff_stddev = coeff_stddev;

end


function mech_prop_reshaped = reshape8by5(mech_prop)
% change 1 dim array to 8x5 matrix row-wise

mech_prop_reshaped = reshape(mech_prop, 5, [])';

end
