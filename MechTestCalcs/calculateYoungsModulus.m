function [youngs_mod_MPa, y_int, max_residual] = calculateYoungsModulus(stress_MPa, strain, plot_dir, alias)

p = polyfit(strain, stress_MPa, 1);
youngs_mod_MPa = p(1);
y_int = p(2);

alias = strrep(alias, '_', '-');
plot_filename = ['YMResid_', alias, '.fig'];
yvals = youngs_mod_MPa .* strain + y_int;
residuals = stress_MPa - yvals;
max_residual = max(abs(residuals));

figure;
subplot(2,1,1)
plot(strain, stress_MPa,'.');hold on
plot(strain, yvals, '-')
xlabel('Strain %')
ylabel('Stress MPa')
xlim([min(strain) max(strain)])
title(['YM linear fit - ', alias])
subplot(2,1,2)
plot(strain, residuals, '.')
xlabel('Strain %')
ylabel('Residuals')
xlim([min(strain) max(strain)])
title('Residual Plot')

savefig([plot_dir, plot_filename])

end