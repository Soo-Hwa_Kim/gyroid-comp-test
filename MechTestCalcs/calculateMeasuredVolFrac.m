function indiv_vol_frac = calculateMeasuredVolFrac(ExperimentalData, this_field)

x = ExperimentalData.IndividualParts.(this_field).mean_x_lengths;
y = ExperimentalData.IndividualParts.(this_field).mean_y_lengths;
z = ExperimentalData.IndividualParts.(this_field).mean_z_lengths;
outer_volume_cm3 = x * y * z / 1000;

weights = ExperimentalData.IndividualParts.(this_field).Weights;
mean_weights_g = mean(weights);
stddev_weights_g = std(weights);
ExperimentalData.IndividualParts.(this_field).meanweightg = mean_weights_g;
ExperimentalData.IndividualParts.(this_field).stddevweightg = stddev_weights_g;

part_density_gcm3 = mean_weights_g / outer_volume_cm3;
filament_density_gcm3 = ExperimentalData.density.meandensitygcm3;
indiv_vol_frac = part_density_gcm3 / filament_density_gcm3;

end