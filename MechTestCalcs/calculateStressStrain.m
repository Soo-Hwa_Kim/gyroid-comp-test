function [stress_MPa, strain] = calculateStressStrain(ThisPart, cross_area_mm2)

compression_array = cell2mat(cellfun(@str2num, ThisPart.Compression, 'un', 0));
force_array_kN = compression_array(:, 2);
displ_array_mm = compression_array(:, 1);
mean_z_length = mean(ThisPart.ZLengths);

stress = (force_array_kN * 1000) / cross_area_mm2;
[comp_start, displ_diff] = findCompressionTestStart(stress, displ_array_mm);
stress_MPa = stress(comp_start:end);
strain = (displ_array_mm(comp_start:end) - displ_diff) / mean_z_length;

end