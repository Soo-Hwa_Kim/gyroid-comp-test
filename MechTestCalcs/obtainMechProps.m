function [youngs_mod_MPa, yield_str_MPa, indiv_vol_frac, ExperimentalData] = obtainMechProps(ExperimentalData)

fields = fieldnames(ExperimentalData.IndividualParts);

youngs_mod_MPa = zeros(length(fields), 1);
yield_str_MPa = zeros(length(fields), 1);
indiv_vol_frac = zeros(length(fields), 1);
max_residuals = zeros(length(fields), 1);

part_counter = 1;

for n = 1:length(fields)
    
    this_field = fields{n};
    compression_array = cell2mat(cellfun(@str2num, ExperimentalData.IndividualParts.(this_field).Compression, 'un', 0));

    [cross_area_mm2, ExperimentalData] = calculateCrossArea(ExperimentalData, this_field);
    [z_mm, ExperimentalData] = getHeight(ExperimentalData, this_field);
    indiv_vol_frac(n) = calculateMeasuredVolFrac(ExperimentalData, this_field);
    lin_window_idx = findLinearWindow(compression_array, ExperimentalData, this_field);
    [stress_MPa, strain] = changeCompDataToStressStrain(compression_array, cross_area_mm2, z_mm);
    [youngs_mod_MPa(n), y_int, max_residuals(n)] = calculateYoungsModulus(stress_MPa(lin_window_idx(1):lin_window_idx(2)), ...
        strain(lin_window_idx(1):lin_window_idx(2)), ExperimentalData.resid_plot_dir, ...
        this_field);
    
    part_idx = mod(part_counter, 5);
    stress_strain_corr_mat = correctToeRegion(lin_window_idx, youngs_mod_MPa(n), y_int, stress_MPa, strain);
    if part_idx == 0
        stress_strain_cell{5} = stress_strain_corr_mat;
        plotCorrectedStressStrain(stress_strain_cell, n, ExperimentalData)
    else
        stress_strain_cell{part_idx} = stress_strain_corr_mat;
    end
    %yield_str_MPa(n) = calculateYieldStrength(stress_MPa_corr, strain_corr, youngs_mod_MPa);
    part_counter = part_counter+1;
    
end

ExperimentalData.max_ym_residual_percent = 100*max(max_residuals./youngs_mod_MPa);

end