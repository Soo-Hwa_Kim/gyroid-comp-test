function yield_strength_MPa = findYieldStrength(stress_MPa, strain, name)

if contains(name, '1_') || contains(name, '2_') || contains(name, '3_') || contains(name, '4_')
    elasticish = floor(0.2 * length(stress_MPa));
    yield_strength_MPa = max(stress_MPa(1:elasticish));
    
elseif contains(name, '5_') || contains(name, '6_')
    elasticish = floor(0.25 * length(stress_MPa));
    
else
    elasticish = floor(0.25 * length(stress_MPa));
    
end



end