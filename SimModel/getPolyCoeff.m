function coeffs = getPolyCoeff(alias)
% coeff{1} for thickness and coeff{2} for volume

coeff_dir = [pwd, '\SimModel\'];
fname_thickness = [alias, 'Coeff', 'Thickness', 'Double', '.txt'];
fname_volfrac = [alias, 'Coeff', 'Volume', 'Double', '.txt'];
coeffs{1} = getFileData(coeff_dir, fname_thickness);
coeffs{2} = getFileData(coeff_dir, fname_volfrac);

end

function data = getFileData(dir, fname)

file_name = fullfile([dir, fname]);
FID = fopen(file_name);
data = textscan(FID,'%s','Delimiter','');
fclose(FID);

end
