function [sim_vol_frac, isovalues] = getSimVolFrac

[alias, struct_type, num_periods, length_mm, input_isovalues] = partInformation;
scale_factor = findScaleFactor(num_periods, length_mm);
coeffs = getPolyCoeff(alias);

for n = 1:length(input_isovalues)
    if n == 1
        this_input_isovalues = input_isovalues(n);
    else
        this_input_isovalues = [-input_isovalues(n), input_isovalues(n)];
    end
    [adjusted_main_isovalues, ~] = adjustMainIsovalues(coeffs{1}, struct_type(n), this_input_isovalues, scale_factor, alias);
    sim_vol_frac(n) = calculateGeomPropPolyfit(coeffs{2}, adjusted_main_isovalues(1), adjusted_main_isovalues(2));
    isovalues(n,:) = adjusted_main_isovalues;
end

end