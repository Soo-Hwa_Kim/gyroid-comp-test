function [alias, struct_type, num_periods, length_mm, input_isovalues] = partInformation
% This was the input information to create the parts for this set of
% compression experimentation. This information was taken from 
% run_makeGCode.m and lab notes.

alias = 'Gyroid';
struct_type = [0 2 2 2 2 2 2 2]';
num_periods = 4;
length_mm = 38;
input_isovalues = [0 0.2 0.4 0.59 0.77 0.93 1.08 1.21];

end