function [sim_youngs_mod_MPa, sim_yield_str_MPa] = calculateSimYMYS(C, n, sim_vol_frac)

sim_youngs_mod_MPa = C(1) .* sim_vol_frac .^ n(1);
sim_yield_str_MPa = 0; %TEMPORARY!!!!

end