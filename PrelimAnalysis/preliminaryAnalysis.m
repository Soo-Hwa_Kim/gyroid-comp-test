function preliminaryAnalysis(PartsData, density_g_cm3)

plot_dir = [pwd, '\PrelimAnalysis\Plots\'];
table_dir = [pwd, '\PrelimAnalysis\Tables\'];
fields = fieldnames(PartsData);
theoretical_vol_frac = [0.128826713, 0.255950163, 0.382018941, 0.501332237, ...
0.614258973, 0.714663707, 0.808933295, 0.890319918];

youngs_mod_MPa = zeros(length(fields)/5, 3);
counter = 1;
part_counter = 1;
for n = 1:length(fields)
    this_field = fields{n};
    compression_array = cell2mat(cellfun(@str2num, PartsData.(this_field).Compression, 'un', 0));
    title_label1 = strrep(this_field, '_', '-');
    
    figure
    plot(compression_array(:,1), compression_array(:,2), '.')
    xlim([0 inf])
    ylim([0 inf])
    xlabel('Displacement mm')
    ylabel('Force kN')
    title(title_label1)
    fname1 = [this_field, '.fig'];
    savefig([plot_dir, fname1]) %save as fig
    
    cross_area_mm2 = calculateCrossArea(PartsData.(this_field));
    cxydxy = PartsData.(this_field).abcd(5:end);
    slope = (cxydxy(4) - cxydxy(2)) / (cxydxy(3) - cxydxy(1));
    this_youngs_mod_MPa = slope / cross_area_mm2 * 1000;
    this_y_int = (cxydxy(2)/ cross_area_mm2 * 1000) - this_youngs_mod_MPa*cxydxy(1);
    this_x_int = -this_y_int / this_youngs_mod_MPa;
    start_data_idxs = find(compression_array(:,1)>=cxydxy(1));
    start_data_idx = min(start_data_idxs);
    if numel(start_data_idx)>1
        keyboard
    end
    adjusted_compression_array = compression_array(start_data_idx:end,:);
    adjusted_compression_array(:,1) = adjusted_compression_array(:,1) - this_x_int;
    adjusted_compression_array(:,2) = adjusted_compression_array(:,2) ./ cross_area_mm2 * 1000;
    adjusted_compression_array = [0 0;adjusted_compression_array];
    
%     figure
%     plot(adjusted_compression_array(:,1), adjusted_compression_array(:,2), '-o')
%     xlim([0 inf])
%     ylim([0 inf])
%     xlabel('Displacement mm')
%     ylabel('Strain MPa')
%     title([title_label1, '-adjusted'])
    

    
    mod_counter = mod(counter, 5); %5 because there are 5 of each part type
    
    if mod_counter == 0
        this_part_type{5} = compression_array;
        youngs_mod_arr(5) = this_youngs_mod_MPa;
        youngs_mod_MPa(part_counter, :) = [part_counter, mean(youngs_mod_arr), std(youngs_mod_arr)];
        this_part_type_adjusted{5} = adjusted_compression_array;
        
        [title_label2, legend_label, fname2, fname3] = getPlotLabels(part_counter);
        
        figure; hold on
        plot(this_part_type{1}(:,1), this_part_type{1}(:,2), '.')
        plot(this_part_type{2}(:,1), this_part_type{2}(:,2), '.')
        plot(this_part_type{3}(:,1), this_part_type{3}(:,2), '.')
        plot(this_part_type{4}(:,1), this_part_type{4}(:,2), '.')
        plot(this_part_type{5}(:,1), this_part_type{5}(:,2), '.')
        xlim([0 inf])
        ylim([0 inf])
        xlabel('Displacement mm')
        ylabel('Force kN')
        title(title_label2)
        legend(legend_label{:})

        savefig([plot_dir, fname2]) %save as fig
        
        figure; hold on
        plot(this_part_type_adjusted{1}(:,1), this_part_type_adjusted{1}(:,2), '-')
        plot(this_part_type_adjusted{2}(:,1), this_part_type_adjusted{2}(:,2), '-')
        plot(this_part_type_adjusted{3}(:,1), this_part_type_adjusted{3}(:,2), '-')
        plot(this_part_type_adjusted{4}(:,1), this_part_type_adjusted{4}(:,2), '-')
        plot(this_part_type_adjusted{5}(:,1), this_part_type_adjusted{5}(:,2), '-')
        xlim([0 inf])
        ylim([0 inf])
        xlabel('Displacement mm')
        ylabel('Strain MPa')
        title([title_label2, ' -adjusted'])
        legend(legend_label{:})
        
        savefig([plot_dir, fname3]) %save as fig
        
        part_counter = part_counter + 1;
        
    else
        this_part_type{mod_counter} = compression_array;
        youngs_mod_arr(mod_counter) = this_youngs_mod_MPa;
        this_part_type_adjusted{mod_counter} = adjusted_compression_array;
    end
    

    
    
    counter = counter + 1;
    
end

fname4 = 'prelim_ym_loglog.fig';
figure;
loglog(theoretical_vol_frac, youngs_mod_MPa(:,2), '.')
xlim([0 1])
ylim([0 inf])
xlabel('Volume fraction')
ylabel('Youngs modulus MPa')
title('Youngs modulus vs. volume fraction')
savefig([plot_dir, fname4]) %save as fig

table_youngs_mod = array2table(youngs_mod_MPa,'VariableNames',{'Type','Mean','StdDev'});
save([table_dir,'table_youngs_mod.mat'], 'table_youngs_mod')

% 
% lvf = log(theoretical_vol_frac);
% lym = log(youngs_mod_MPa)';
% p = polyfit(lvf, lym, 1);
% vfx = logspace(0,1,100);
% py = polyval(p, vfx);
% pym = polyval(p, lvf);
% resids = pym - lym;
% 
% figure;
% plot(lvf, lym, 'o'); hold on
% plot(vfx, py, '-')
% legend('vals', 'polyfit 1 deg')
% xlim([0 1])
% ylim([0 inf])
% xlabel('Volume fraction')
% ylabel('Youngs modulus MPa')
% title('Youngs modulus vs. volume fraction with fit')
% 
% figure;
% plot(lym, resids, 'o'); hold on
% plot(lym, zeros(size(lym)), '-')
% xlim([0 1])
% xlabel('log(Volume fraction)')
% ylabel('Residuals')
% title('Residual plot of polyfit')

end

function [title_label, legend_label, fname2, fname3] = getPlotLabels(part_counter)

title_label = sprintf('Part Type %i', part_counter);
fname2 = [sprintf('ALLPartType%i', part_counter), '.fig'];
fname3 = [sprintf('ADJUSTEDPartType%i', part_counter), '.fig'];

legend_label1 = sprintf('%i-1', part_counter);
legend_label2 = sprintf('%i-2', part_counter);
legend_label3 = sprintf('%i-3', part_counter);
legend_label4 = sprintf('%i-4', part_counter);
legend_label5 = sprintf('%i-5', part_counter);

legend_label = {legend_label1, legend_label2, legend_label3, ...
    legend_label4, legend_label5};

end