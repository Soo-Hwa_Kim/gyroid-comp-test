function [C, n, coeff_stddev] = getGAConst(mech_prop, vol_frac)

fun = @(x)x(1) .* vol_frac .^ (x(2)) - mech_prop; %Gibson-Ashby relation
x0 = [mech_prop(end)/vol_frac(end), 1];
[x,~,~,~,~,~,jacobian] = lsqnonlin(fun, x0);
C = x(1);
n = x(2);

jacobian = full(jacobian);
covar = inv(jacobian' * jacobian);
coeff_stddev = sqrt(diag(covar));

end