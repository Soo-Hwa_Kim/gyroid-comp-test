function [sim_youngs_mod_MPa, sim_yield_str_MPa, sim_vol_frac, isovalues] = calculateSimVals(C, n)

[sim_vol_frac, isovalues] = getSimVolFrac;
[sim_youngs_mod_MPa, sim_yield_str_MPa] = calculateSimYMYS(C, n, sim_vol_frac);

end