function compareExptSim(C, n, ExperimentalData)

yield_str_MPa = 0; %TEMPORARY PLACEHOLDER

[sim_youngs_mod_MPa, sim_yield_str_MPa, sim_vol_frac, isovalues] = calculateSimVals(C, n);

comparisonAnalysis(sim_youngs_mod_MPa, sim_yield_str_MPa, sim_vol_frac, ...
    isovalues, ExperimentalData.YoungsModulusMPa.mean_YM, yield_str_MPa, ...
    ExperimentalData.VolumeFraction.mean_VF, ExperimentalData);

end