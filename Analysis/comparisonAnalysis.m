function comparisonAnalysis(sim_youngs_mod_MPa, sim_yield_str_MPa, sim_vol_frac, ...
    isovalues, youngs_mod_MPa, yield_str_MPa, vol_frac, ExperimentalData)

plot_isovals = isovalues(:,2);
% Es = ExperimentalData.GAConstants.Es;
% sim_youngs_mod_MPa = sim_youngs_mod_MPa * Es;
norm_delta_vol_frac = calculateNormDelta(vol_frac, sim_vol_frac);
norm_delta_youngs_mod = calculateNormDelta(youngs_mod_MPa, sim_youngs_mod_MPa);



fig1 = figure;
plot(plot_isovals, vol_frac, '.', 'MarkerSize', 20);hold on
plot(plot_isovals, sim_vol_frac, '.', 'MarkerSize', 20)
xlim([0 inf])
ylim([0 1])
xlabel('Isovalue')
ylabel('Volume Fraction')
title('Volume Fraction vs. Isovalue')
legend('Measured', 'Simulated', 'Location', 'northwest')

savefig([ExperimentalData.comp_plot_dir, 'VvsI.fig']);
set(fig1,'Units', 'Inches');
pos = get(fig1, 'Position');
set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig1, [ExperimentalData.comp_plot_dir, 'VvsI.pdf'], '-dpdf', '-r0') %save as pdf

fig2 = figure;
plot(plot_isovals, youngs_mod_MPa, '.', 'MarkerSize', 20);hold on
plot(plot_isovals, sim_youngs_mod_MPa, '.', 'MarkerSize', 20)
xlim([0 inf])
ylim([0 inf])
xlabel('Isovalue')
ylabel('Youngs Modulus (MPa)')
title('Youngs Modulus vs. Isovalue')
legend('Measured', 'Simulated', 'Location', 'northwest')

savefig([ExperimentalData.comp_plot_dir, 'EvsI.fig']);
set(fig2,'Units', 'Inches');
pos = get(fig2, 'Position');
set(fig2, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig2, [ExperimentalData.comp_plot_dir, 'EvsI.pdf'], '-dpdf', '-r0') %save as pdf


fig3 = figure;
plot(plot_isovals, norm_delta_vol_frac, '.', 'MarkerSize', 20);hold on
xlabel('Isovalue')
ylabel('Normalised Delta Vol Frac')
title('Volume Fraction Error vs. Isovalue')

savefig([ExperimentalData.comp_plot_dir, 'VErrorvsI.fig']);
set(fig3,'Units', 'Inches');
pos = get(fig3, 'Position');
set(fig3, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig3, [ExperimentalData.comp_plot_dir, 'VErrorvsI.pdf'], '-dpdf', '-r0') %save as pdf

fig4 = figure;
plot(plot_isovals, norm_delta_youngs_mod, '.', 'MarkerSize', 20);hold on
xlabel('Isovalue')
ylabel('Normalised Delta Youngs Mod')
title('Youngs Mod Error vs. Isovalue')

savefig([ExperimentalData.comp_plot_dir, 'EErrorvsI.fig']);
set(fig4,'Units', 'Inches');
pos = get(fig4, 'Position');
set(fig4, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig4, [ExperimentalData.comp_plot_dir, 'EErrorvsI.pdf'], '-dpdf', '-r0') %save as pdf

end

function norm_delta = calculateNormDelta(meas, sim)
if size(meas) ~= size(sim)
    sim = sim';
end

delta = meas - sim;
norm_delta = delta ./ meas;

end