function expected_vals = calcExpectedMass(isovals, alias, scale_mm, pla_density_gcm3)

if nargin < 4
    pla_density_gcm3 = 1.24; %online prusa pla filament density g/cm^3
end
mass_solid_cube_pla_g = findMassSolidCube(scale_mm, pla_density_gcm3);
scale_factor = findScaleFactor(4, scale_mm);
[vol_coeffs, thickness_coeffs] = getCoeffsForMassCalc(alias);

for n = 1:size(isovals,1)
    this_isovals = isovals(n,:);
%     if n == size(isovals,1)
%         keyboard
%     end
    [adjusted_main_isovalues(n,:), ~] = adjustMainIsovalues(thickness_coeffs, 2, this_isovals, scale_factor, alias);
    expected_vol_frac(n,:) = calculateGeomPropPolyfit(vol_coeffs, adjusted_main_isovalues(n,1), adjusted_main_isovalues(n,2));
    expected_mass_g(n,:) = expected_vol_frac(n) * mass_solid_cube_pla_g;
end

expected_vals = [adjusted_main_isovalues,...
    expected_vol_frac,...
    expected_mass_g];

keyboard

end

function mass_solid_cube_pla_g = findMassSolidCube(scale_mm, pla_density_gcm3)
filament_thickness = 0.35;
vol_cm3 = ((scale_mm + filament_thickness)/10)^3;
mass_solid_cube_pla_g = pla_density_gcm3 * vol_cm3;

end

function [vol_coeffs, thickness_coeffs] = getCoeffsForMassCalc(alias)

coeff_dir = [pwd, '\FittingCoefficients\', alias, '\'];

fname = [alias, 'Coeff', 'Volume', 'Double', '.txt'];

file_name = fullfile([coeff_dir, fname]);
FID = fopen(file_name);
this_coeff = textscan(FID,'%s','Delimiter','');
fclose(FID);
vol_coeffs = this_coeff{:};

fname = [alias, 'Coeff', 'Thickness', 'Double', '.txt'];

file_name = fullfile([coeff_dir, fname]);
FID = fopen(file_name);
this_coeff = textscan(FID,'%s','Delimiter','');
fclose(FID);
thickness_coeffs = this_coeff{:};


end